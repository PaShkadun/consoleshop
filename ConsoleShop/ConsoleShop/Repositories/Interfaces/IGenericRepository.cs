﻿using System.Collections.Generic;

namespace ConsoleShop.Repositories.Interfaces
{
    public interface IGenericRepository<TEntity>
    {
        bool Add(TEntity entity);
        List<TEntity> GetAll();
    }
}
