﻿using ConsoleShop.Models;
using System.Collections.Generic;

namespace ConsoleShop.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        void MakeOrder(Basket basket, string id);
        List<Basket> GetAllUserOrders(string id);
    }
}
