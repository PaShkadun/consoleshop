﻿using ConsoleShop.Repositories.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleShop.Repositories.Implementations
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private const string FilePath = "../../../Datas/{0}.json";

        public bool Add(TEntity entity)
        {
            var entities = GetAll();
            entities.Add(entity);

            using (var file = new FileStream(string.Format(FilePath, typeof(TEntity).Name), FileMode.OpenOrCreate))
            {
                JsonSerializer.SerializeAsync(file, entities);
            }

            return true;
        }

        public List<TEntity> GetAll()
        {
            var entities = new List<TEntity>();

            if (File.Exists(string.Format(FilePath, typeof(TEntity).Name)))
            {
                var valueString = File.ReadAllText(string.Format(FilePath, typeof(TEntity).Name));

                if (!string.IsNullOrEmpty(valueString))
                {
                    entities = JsonSerializer.Deserialize<List<TEntity>>(valueString);
                }
            }

            return entities;
        }
    }
}
