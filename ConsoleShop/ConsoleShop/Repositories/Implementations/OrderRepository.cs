﻿using ConsoleShop.Models;
using ConsoleShop.Repositories.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleShop.Repositories.Implementations
{
    public class OrderRepository : IOrderRepository
    {
        private readonly string userBasketPath = "../../../Datas/{0}.json";

        public void MakeOrder(Basket basket, string userId)
        {
            var orders = GetAllUserOrders(userId);
            orders.Add(basket);

            using (var file = new FileStream(string.Format(userBasketPath, userId), FileMode.Create))
            {
                JsonSerializer.SerializeAsync(file, orders);
            }
        }

        public List<Basket> GetAllUserOrders(string userId)
        {
            var orders = new List<Basket>();

            if (File.Exists(string.Format(userBasketPath, userId)))
            {
                string ordersHistory = File.ReadAllText(string.Format(userBasketPath, userId));

                if (!string.IsNullOrEmpty(ordersHistory))
                {
                    orders = JsonSerializer.Deserialize<List<Basket>>(ordersHistory);
                }
            }

            return orders;
        }
    }
}
