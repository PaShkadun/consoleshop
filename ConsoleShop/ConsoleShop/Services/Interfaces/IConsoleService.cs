﻿namespace ConsoleShop.Services.Interfaces
{
    public interface IConsoleService
    {
        void ShowMessage(string message);
        string InputStringValue(string message);
        void StopConsole();
        int InputIntValue(string message);
    }
}
