﻿using System.Timers;

namespace ConsoleShop.Services.Interfaces
{
    public interface IShopService
    {
        void MakeOrder(string id);
        void AddProductToBasket(int productIndex, int count);
        void DeleteProductFromBasket(int index);
        void ShowProducts();
        void ShowBasketProducts();
        void ShowOrderHistory(string userId);
    }
}
