﻿using ConsoleShop.Models;

namespace ConsoleShop.Services.Interfaces
{
    public interface IAuthorizationService
    {
        User Login(string login, string password);
        User Register(string login, string password);
    }
}
