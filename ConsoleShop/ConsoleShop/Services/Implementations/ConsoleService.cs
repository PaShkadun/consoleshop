﻿using ConsoleShop.Constants;
using ConsoleShop.Services.Interfaces;
using System;

namespace ConsoleShop.Services.Implementations
{
    public class ConsoleService : IConsoleService
    {
        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }

        public string InputStringValue(string message)
        {
            Console.WriteLine(message);

            return Console.ReadLine();
        }

        public int InputIntValue(string message)
        {
            Console.WriteLine(message);

            int value;

            while (!int.TryParse(Console.ReadLine(), out value) || value < 0)
            {
                Console.WriteLine(StringConstants.IncorrectInput);
            }

            return value;
        }

        public void StopConsole()
        {
            Console.WriteLine(StringConstants.PressAnyKey);
            Console.ReadKey(true);
            Console.Clear();
        }
    }
}
