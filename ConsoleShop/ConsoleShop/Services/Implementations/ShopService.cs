﻿using System;
using ConsoleShop.Constants;
using ConsoleShop.Models;
using ConsoleShop.Repositories.Interfaces;
using ConsoleShop.Services.Interfaces;
using System.Collections.Generic;
using System.Timers;

namespace ConsoleShop.Services.Implementations
{
    public class ShopService : IShopService
    {
        private readonly IGenericRepository<Product> productRepository;
        private readonly IConsoleService consoleService;
        private readonly IOrderRepository orderRepository;
        private readonly Basket basket;
        private readonly Timer timer;
        private List<Product> Products { get; set; }

        public ShopService(
            IGenericRepository<Product> productRepository,
            IConsoleService consoleService,
            IOrderRepository orderRepository,
            Basket basket,
            Timer timer)
        {
            this.productRepository = productRepository;
            this.consoleService = consoleService;
            this.orderRepository = orderRepository;
            this.basket = basket;
            this.timer = timer;

            timer.Elapsed += ClearBasket;
        }
        public void MakeOrder(string id)
        {
            if (basket.Products.Count != 0)
            {
                orderRepository.MakeOrder(basket, id);
                basket.Products.Clear();
                consoleService.ShowMessage(StringConstants.Success);
                timer.Stop();
            }
            else
            {
                consoleService.ShowMessage(StringConstants.EmptyBasket);
                consoleService.StopConsole();
            }
        }

        public void AddProductToBasket(int productIndex, int count)
        {
            if (Products.Count < productIndex)
            {
                consoleService.ShowMessage(StringConstants.IncorrectIndex);
            }
            else if (Products[productIndex].Count < count)
            {
                consoleService.ShowMessage(StringConstants.TooManyItems);
            }
            else
            {
                if (basket.Products.Count == 0)
                {
                    timer.Start();
                }

                basket.AddProduct(Products[productIndex], count);
                Products[productIndex].Count -= count;

                consoleService.ShowMessage(StringConstants.Success);
            }
        }

        public void DeleteProductFromBasket(int index)
        {
            if (index >= basket.Products.Count || index < 0)
            {
                consoleService.ShowMessage(StringConstants.IncorrectIndex);
            }
            else
            {
                basket.DeleteProduct(index);

                if (basket.Products.Count == 0)
                {
                    timer.Stop();
                }

                consoleService.ShowMessage(StringConstants.Success);
            }
        }

        public void ShowProducts()
        {
            if (Products is null)
            {
                Products = productRepository.GetAll();
            }

            if (Products.Count == 0)
            {
                consoleService.ShowMessage(StringConstants.NopeProduct);
            }
            else
            {
                foreach (var product in Products)
                {
                    consoleService.ShowMessage($"{product.Name}\t{product.Count}\t{product.Price}\t{product.Description}");
                }
            }
        }

        public void ShowBasketProducts()
        {
            if (basket.Products.Count == 0)
            {
                consoleService.ShowMessage(StringConstants.NopeProduct);
            }
            else
            {
                foreach (var product in basket.Products)
                {
                    consoleService.ShowMessage($"{product.Name}\t{product.Count}\t{product.Price * product.Count}\t{product.Description}");
                }
            }
        }

        public void ShowOrderHistory(string userId)
        {
            var orders = orderRepository.GetAllUserOrders(userId);

            if (orders.Count == 0)
            {
                consoleService.ShowMessage(StringConstants.NoOrders);
            }
            else
            {
                foreach (var order in orders)
                {
                    foreach (var product in order.Products)
                    {
                        consoleService.ShowMessage($"{product.Id} {product.Name} {product.Price * product.Count} {product.Description}");
                    }
                }
            }
        }

        private void ClearBasket(object source, ElapsedEventArgs e)
        {
            basket.Products.Clear();
            consoleService.ShowMessage(StringConstants.ClearBasket);
            timer.Stop();
        }
    }
}
