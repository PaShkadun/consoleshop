﻿using ConsoleShop.Constants;
using ConsoleShop.Models;
using ConsoleShop.Repositories.Interfaces;
using ConsoleShop.Services.Interfaces;
using ConsoleShop.Validators;
using System;
using System.Linq;

namespace ConsoleShop.Services.Implementations
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IGenericRepository<User> repository;
        private readonly IConsoleService console;

        public AuthorizationService(IGenericRepository<User> repository, IConsoleService console)
        {
            this.repository = repository;
            this.console = console;
        }

        public User Login(string login, string password)
        {
            if (!UserValidator.UsernameValidator(login))
            {
                return null;
            }

            if (!UserValidator.PasswordValidator(password))
            {
                return null;
            }

            var users = repository.GetAll();

            if (users.Exists(x => x.Login == login))
            {
                if (users.FirstOrDefault(x => x.Login == login && x.Password == password) is User user)
                {
                    return user;
                }
            }

            console.ShowMessage(StringConstants.LoginNotExists);

            return null;
        }

        public User Register(string login, string password)
        {
            if (!UserValidator.UsernameValidator(login))
            {
                return null;
            }

            if (!UserValidator.PasswordValidator(password))
            {
                return null;
            }

            var users = repository.GetAll();

            if (users.Exists(x => x.Login == login))
            {
                console.ShowMessage(StringConstants.LoginNotAllow);

                return null;
            }

            var newUser = new User
            {
                Id = Guid.NewGuid(),
                Login = login,
                Password = password
            };

            repository.Add(newUser);

            return newUser;
        }
    }
}
