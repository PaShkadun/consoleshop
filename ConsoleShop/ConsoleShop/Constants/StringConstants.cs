﻿namespace ConsoleShop.Constants
{
    public static class StringConstants
    {
        public const string ChooseLoginAction = "1. Login\n2. Register\n3. Exit";
        public const string IncorrectInput = "Incorrect input. Please, don't do it again!";
        public const string InputLogin = "Input login (only digits and letters. >= 4 symbols.";
        public const string InputPassword = "Input password. >= 6 symbols.";
        public const string ByeMessage = "Bye-bye. Come in again!";
        public const string IncorrectLogin = "You inputed incorrect login.";
        public const string IncorrectPassword = "You inputed incorrect password.";
        public const string LoginNotAllow = "This login has already used.";
        public const string LoginNotExists = "This login hasn't used yet.";
        public const string PressAnyKey = "Press any key to continue...";
        public const string NopeProduct = "No one product is exists.";
        public const string Success = "Operation complited succesfully";
        public const string IncorrectIndex = "Index less than zero or more than count.";
        public const string EmptyBasket = "Basket is empty.";
        public const string ChooseShopAction = "1. Show products.\n2. Show products into basket.\n0. Exit\n";
        public const string TooManyItems = "There aren't too many items in shop.";
        public const string ChooseCatalogAction = "1. Add item to basket.\nOther key - exit back.";
        public const string ChooseBasketAction = "1. Make order.\n2. Delete item from basket.\n3. Show orders history\nOther key - exit back.";
        public const string InputIndex = "Input index (>= 0)";
        public const string InputCount = "Input count (>= 0)";
        public const string NoOrders = "You have no orders yet.";
        public const string ConfigurationPath = "/../../../appconf.json";
        public const string ClearBasket = "Busked was cleared.";
    }
}
