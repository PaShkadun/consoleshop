﻿using System;
using ConsoleShop.Models;
using ConsoleShop.Repositories.Implementations;
using ConsoleShop.Services.Implementations;
using Microsoft.Extensions.Configuration;
using System.Timers;
using System.Text;
using ConsoleShop.Constants;

namespace ConsoleShop
{
    public class Program
    {

        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder().AddJsonFile(Environment.CurrentDirectory + StringConstants.ConfigurationPath).Build();

            UserInterface userInterface = new UserInterface(
                new ConsoleService(), 
                new AuthorizationService(
                    new GenericRepository<User>(), 
                    new ConsoleService()), 
                new ShopService(
                    new GenericRepository<Product>(), 
                    new ConsoleService(), 
                    new OrderRepository(), 
                    new Basket(),
                    new Timer(configuration.GetValue<int>("timer:time")))); 

            userInterface.CreateLoginInterface();
        }
    }
}
