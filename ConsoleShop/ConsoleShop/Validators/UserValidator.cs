﻿using ConsoleShop.Constants;
using ConsoleShop.Services.Implementations;
using ConsoleShop.Services.Interfaces;

namespace ConsoleShop.Validators
{
    public static class UserValidator
    {
        private static readonly IConsoleService ConsoleService = new ConsoleService();

        public static bool UsernameValidator(string login)
        {
            if (login is null || login.Length < 4)
            {
                ConsoleService.ShowMessage(StringConstants.IncorrectLogin);

                return false;
            }

            for (var i = 0; i < login.Length; i++)
            {
                if (!char.IsLetterOrDigit(login[i]))
                {
                    ConsoleService.ShowMessage(StringConstants.IncorrectLogin);

                    return false;
                }
            }

            return true;
        }

        public static bool PasswordValidator(string password)
        {
            if (password is null || password.Length < 6)
            {
                ConsoleService.ShowMessage(StringConstants.IncorrectPassword);

                return false;
            }

            return true;
        }
    }
}
