﻿using ConsoleShop.Constants;
using ConsoleShop.Models;
using ConsoleShop.Services.Interfaces;
using System;

namespace ConsoleShop
{
    public class UserInterface
    {
        private readonly IConsoleService consoleService;
        private readonly IAuthorizationService authorization;
        private readonly IShopService shopService;
        private User user;

        public UserInterface(IConsoleService consoleService, IAuthorizationService authorization, IShopService shopService)
        {
            this.consoleService = consoleService;
            this.authorization = authorization;
            this.shopService = shopService;
        }

        public void CreateLoginInterface()
        {
            while (user is null)
            {
                consoleService.ShowMessage(StringConstants.ChooseLoginAction);

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        user = authorization.Login(
                            consoleService.InputStringValue(StringConstants.InputLogin),
                            consoleService.InputStringValue(StringConstants.InputPassword));

                        break;

                    case ConsoleKey.D2:
                        user = authorization.Register(
                            consoleService.InputStringValue(StringConstants.InputLogin),
                            consoleService.InputStringValue(StringConstants.InputPassword));

                        break;

                    case ConsoleKey.D3:
                        consoleService.ShowMessage(StringConstants.ByeMessage);

                        return;
                }

                consoleService.StopConsole();
            }

            CreateShopInterface();
        }

        public void CreateShopInterface()
        {
            while (true)
            {
                consoleService.ShowMessage(StringConstants.ChooseShopAction);

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        CreateCatalogInterface();

                        break;

                    case ConsoleKey.D2:
                        CreateBasketInterface();

                        break;

                    case ConsoleKey.D0:
                        return;
                }

                consoleService.StopConsole();
            }
        }

        public void CreateCatalogInterface()
        {
            while (true)
            {
                shopService.ShowProducts();
                consoleService.ShowMessage(StringConstants.ChooseCatalogAction);

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        var index = consoleService.InputIntValue(StringConstants.InputIndex);
                        var count = consoleService.InputIntValue(StringConstants.InputCount);

                        shopService.AddProductToBasket(index, count);

                        break;

                    default:
                        return;
                }

                consoleService.StopConsole();
            }
        }

        public void CreateBasketInterface()
        {
            while (true)
            {
                shopService.ShowBasketProducts();
                consoleService.ShowMessage(StringConstants.ChooseBasketAction);

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        shopService.MakeOrder(user.Id.ToString());

                        break;

                    case ConsoleKey.D2:
                        var index = consoleService.InputIntValue(StringConstants.InputIndex);

                        shopService.DeleteProductFromBasket(index);

                        break;

                    case ConsoleKey.D3:
                        shopService.ShowOrderHistory(user.Id.ToString());

                        break;

                    default:
                        return;
                }

                consoleService.StopConsole();
            }
        }
    }
}
