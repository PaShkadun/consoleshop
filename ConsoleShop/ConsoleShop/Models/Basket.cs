﻿using System.Collections.Generic;
using System.Linq;

namespace ConsoleShop.Models
{
    public class Basket
    {
        public List<Product> Products { get; set; }

        public Basket()
        {
            Products = new List<Product>();
        }

        public void AddProduct(Product product, int count)
        {
            if (Products.Exists(prod => prod.Id == product.Id))
            {
                Products.First(prod => prod.Id == product.Id).Count += count;
            }
            else
            {
                Products.Add(new Product
                {
                    Id = product.Id,
                    Name = product.Name,
                    Description = product.Description,
                    Price = product.Price,
                    Count = count
                });
            }
        }

        public void DeleteProduct(int index)
        {
            Products.RemoveAt(index);
        }
    }
}
